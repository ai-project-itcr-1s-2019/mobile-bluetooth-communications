package jamh.arduino;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main extends AppCompatActivity {

    private Button refreshButton;
    private ListView devicesList;
    private TextView text;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayAdapter<String> arrayAdapter;
    List<String> dispositivos = new  ArrayList<String>();
    public static String EXTRA_DEVICE_ADDRESS  = "device_address";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        refreshButton = findViewById(R.id.refreshButton);
        text = findViewById(R.id.textDevices);
        devicesList = findViewById(R.id.diviceList);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchDevices();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        verifyBtState();

        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, dispositivos);
        devicesList.setAdapter(arrayAdapter);
        devicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "Conectando...", Toast.LENGTH_LONG).show();

                String info = ((TextView) view).getText().toString();
                String address = info.substring(info.length()-17);
                Intent i = new Intent(Main.this, ArduinoControl.class);
                i.putExtra(EXTRA_DEVICE_ADDRESS, address);
                startActivity(i);
            }
        });

        searchDevices();
    }

    private void searchDevices()
    {
        dispositivos.clear();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if(pairedDevices.size()>0)
        {
            for (BluetoothDevice device :pairedDevices)
            {
                dispositivos.add(device.getName()+"\n"+device.getAddress());
            }
        }
    }

    private void verifyBtState()
    {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(bluetoothAdapter==null)
        {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        }
        else
        {
            if(bluetoothAdapter.isEnabled())
            {
                Toast.makeText(getBaseContext(), "Dispositivo conectado", Toast.LENGTH_LONG).show();
            }
            else
            {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }
}
