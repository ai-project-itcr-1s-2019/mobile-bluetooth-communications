package jamh.arduino;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class ArduinoControl extends AppCompatActivity {
    private ArduinoController arduinoController;


    class C02412 implements OnClickListener {
        C02412() {
        }
        public void onClick(View v) {
            ArduinoControl.this.arduinoController.closeConnection();
        }
    }

    class enviarDato implements OnClickListener {
        private String dato;
        enviarDato(String dato) {
            this.dato = dato;
        }
        public void onClick(View v) {
            ArduinoControl.this.arduinoController.write(this.dato);
            Toast.makeText(ArduinoControl.this, "Enviando dato:"+dato, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressLint({"ClickableViewAccessibility"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arduino_control);
        ((Button) findViewById(R.id.button4)).setOnClickListener(new C02412());
        ((Button) findViewById(R.id.leftButton)).setOnClickListener(new enviarDato("L"));
        ((Button) findViewById(R.id.rightButton)).setOnClickListener(new enviarDato("R"));
        ((Button) findViewById(R.id.forwardButton)).setOnClickListener(new enviarDato("F"));
        ((Button) findViewById(R.id.backButton)).setOnClickListener(new enviarDato("B"));
        ((Button) findViewById(R.id.stopbutton)).setOnClickListener(new enviarDato("S"));

    }


    protected void onResume() {
        super.onResume();
        String address = getIntent().getStringExtra(Main.EXTRA_DEVICE_ADDRESS);
        Toast.makeText(this, address, Toast.LENGTH_SHORT).show();
        this.arduinoController = ArduinoController.ArduinoControllerInstance(this, address);
        this.arduinoController.initialize();
    }

    public void onPause() {
        super.onPause();
        this.arduinoController.closeConnection();
    }
}
