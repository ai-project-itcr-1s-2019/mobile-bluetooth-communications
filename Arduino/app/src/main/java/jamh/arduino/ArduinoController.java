package jamh.arduino;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.widget.Toast;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class ArduinoController {
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static ArduinoController singleton_instance = null;
    private String ARDUINO_MAC = null;
    private ArduinoControl arduinoControl;
    private BluetoothSocket bluetoothSocket;
    private OutputStream outputStream;

    private ArduinoController(String ARDUINO_MAC, ArduinoControl arduinoControl) {
        this.ARDUINO_MAC = ARDUINO_MAC;
        this.arduinoControl = arduinoControl;
    }

    public static ArduinoController ArduinoControllerInstance(ArduinoControl arduinoControl, String MAC) {
        ArduinoController arduinoController = singleton_instance;
        if (arduinoController != null) {
            return arduinoController;
        }
        Toast.makeText(arduinoControl, "I'm initializing", Toast.LENGTH_SHORT).show();
        singleton_instance = new ArduinoController(MAC, arduinoControl);
        return singleton_instance;
    }

    public boolean initialize() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this.arduinoControl, "The device does't have bluetooth", Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(this.ARDUINO_MAC);
            device.getName();
            this.bluetoothSocket = createBluetoothSocket(device);
            try {
                this.outputStream = this.bluetoothSocket.getOutputStream();
                try {
                    this.bluetoothSocket.connect();
                } catch (IOException e) {
                    try {
                        this.bluetoothSocket.close();
                    } catch (IOException e2) {
                        Toast.makeText(this.arduinoControl, "The connection can't be established 3", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
                return true;
            } catch (IOException e3) {
                Toast.makeText(this.arduinoControl, "The connection can't be established 2", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (IOException e4) {
            Toast.makeText(this.arduinoControl, "The connection can't be established 1", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    public boolean write(String command) {
        try {
            this.outputStream.write(command.getBytes());
            return true;
        } catch (IOException e) {
            Toast.makeText(this.arduinoControl, "The connection can't be established write", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean closeConnection() {
        try {
            this.bluetoothSocket.close();
            return true;
        } catch (IOException e) {
            Toast.makeText(this.arduinoControl, "The connection can't be established closing", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
